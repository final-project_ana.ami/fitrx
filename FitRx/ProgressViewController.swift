//
//  ProgressViewController.swift
//  FitRx
//
//  Created by Pragya  Lamsal on 4/12/20.
//  Copyright © 2020 Ami  Lamsal. All rights reserved.
//

import UIKit
import MobileCoreServices
class ProgressViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    @IBOutlet weak var imageView: UIImageView!
    var newMedia: Bool?
    
    
    @IBAction func useCamera(_ sender: AnyObject) {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
            
          
        }
    
    @IBAction func useImageLibrary(_ sender: AnyObject) {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                       let myPickerController = UIImagePickerController()
                       myPickerController.delegate = self;
                       myPickerController.sourceType = .photoLibrary
                self.present(myPickerController, animated: true, completion: nil)
            }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    


}

