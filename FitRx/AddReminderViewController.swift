//
//  AddReminderViewController.swift
//  FitRx
//
//  Created by studentadmin on 4/28/20.
//  Copyright © 2020 Ami  Lamsal. All rights reserved.
//

import UIKit

class AddReminderViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var bodyField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var completion: ((String, String, Date) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleField.delegate = self
        bodyField.delegate = self
    }
    
    @IBAction func didTapSaveButton(_ sender: UIBarButtonItem) {
        if let titleText = titleField.text, !titleText.isEmpty,
            let bodyText = bodyField.text, !bodyText.isEmpty {
        
            let targetDate = datePicker.date
        
            completion?(titleText, bodyText, targetDate)
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
